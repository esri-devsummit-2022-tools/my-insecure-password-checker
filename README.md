# my-insecure-password-checker

A demonstration password checker, used to illustrate GitLab CI/CD pipeline flow.

## IMPORTANT
This is not a secure password checker and should not be used in production. It is for demonstration purposes only
