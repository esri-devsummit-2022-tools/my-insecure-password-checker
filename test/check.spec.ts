import { checkPassword } from "../src";

describe("test validate password", () => {
    it("should return false for invalid username", () => {
        expect(checkPassword("notAName", "Password")).toBe(false);
    });

    it("should return false for invalid password", () => {
        expect(checkPassword("Michael", "notthepassword")).toBe(false);
    });

    it("should return true for valid username and password", () => {
        expect(checkPassword("Michael", "Password")).toBe(true);
        expect(checkPassword("Nathan", "Password")).toBe(true);
        expect(checkPassword("Alex", "Password")).toBe(true);
        expect(checkPassword("Barbara", "Password")).toBe(true);
    });
});
